package com.example.tesnus.testex2;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class SharedPreferencesHelper {

    public static final String SHARED_PREF_NAME = "SHARED_PREF_NAME";
    public static final String RADIO_BTN_KEY = "RADIO_BTN_KEY";

    private SharedPreferences mSharedPreferences;
    private Gson mGson = new Gson();

    public SharedPreferencesHelper(Context context) {
        mSharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public boolean saveRadioBtnState(int radioBtnIndex){
        mSharedPreferences.edit().putInt(RADIO_BTN_KEY, radioBtnIndex).apply();
        return true;
    }

    public int getRadioBtnState(){
        return mSharedPreferences.getInt(RADIO_BTN_KEY, 0);
    }


}
