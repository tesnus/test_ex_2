package com.example.tesnus.testex2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.zip.Inflater;

public class SettingsFragment extends Fragment {

    RadioGroup radioGroup;
    SharedPreferencesHelper mSharedPreferencesHelper;

    public static SettingsFragment newInstance() {return new SettingsFragment(); }


    RadioGroup.OnCheckedChangeListener mRadioGroupOnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            //RadioButton checkedRadioButton = radioGroup.findViewById(checkedId);

            mSharedPreferencesHelper.saveRadioBtnState(checkedId);

            //showMessage(checkedIndex);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fr_settings, container, false);

        mSharedPreferencesHelper = new SharedPreferencesHelper(getActivity());

        radioGroup.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(mRadioGroupOnCheckedChangeListener);

        return view;
    }

    /*private void showMessage(int string){
        Toast.makeText(getActivity(), string, Toast.LENGTH_LONG).show();
    }*/
}
