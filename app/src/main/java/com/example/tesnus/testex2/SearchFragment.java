package com.example.tesnus.testex2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class SearchFragment extends Fragment {

    private EditText mTextForSearch;
    private Button mSearchButton;
    private SharedPreferencesHelper mSharedPreferencesHelper;
    private String google = "google.com";

    public static SearchFragment newInstance() {return new SearchFragment(); }


    public View.OnClickListener mOpenSearchLink = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            Uri uri = Uri.parse("http://www."+ google +"/search?q=" + mTextForSearch.getText().toString());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fr_search, container, false);
        mSharedPreferencesHelper = new SharedPreferencesHelper(getActivity());

        mTextForSearch = view.findViewById(R.id.etForSearch);
        mSearchButton = view.findViewById(R.id.btnSearch);
        mSearchButton.setOnClickListener(mOpenSearchLink);


        return view;
    }
}
